const User = require("../models/User");
const Order = require("../models/Order");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

/* Create Product */
module.exports.createProduct = async (reqBody, userData) => {
	if(userData.isAdmin){
		let newProduct = new Product ({
			productName : reqBody.productName,
			description : reqBody.description,
			price : reqBody.price
		})
		return newProduct.save().then((user, error) => {
			if(error){
				return "Product register fail";
			} else {
				return "Product register success";
			}
		})
	} else {
		return "Not authorized"
	}
	
}

/* Retrieve All Active Products */
module.exports.getAllProducts = () => {
	return Product.find({isActive : true}).then(result => {
		if(result === undefined || result === null) {
	            return "No Products"
	        } else {
	            return result;
	        }
	})
}

/* Retrieve Specific Product */
module.exports.getProduct = (reqParams) => {
	return Product.findOne({_id : reqParams.productId}).then(result => {
		return result;
	}).catch((error) => {
		return `Product with ID: ${reqParams.productId} not existent.`
	})
}

/* Update Product */
module.exports.updateProduct = async (userData, reqParams) => {
	if(userData.isAdmin){
		return Product.findOne({_id : reqParams.productId}).then(result => {
			result.description = "description updated";
			return result.save();
		}).catch((error) => {
			return `Product with ID ${reqParams.productId} not existent.`
		})
	} else {
		return "Not authorized"
	}
}

/* Archive Product */
module.exports.archiveProduct = async (userData, reqParams) => {
	if(userData.isAdmin){
		return Product.findByIdAndUpdate({_id : reqParams.productId}, {isActive: false}).then(result => {
			return result.save().then((updatedResult, error) => {
				if(error){
					return "Error: Archiving the product failed"
				} else {
					return updatedResult;
				}
			});
		}).catch((error) => {
			return `Product with ID ${reqParams.productId} not existent.`
		})
	} else {
		return "Not authorized"
	}
}

const User = require("../models/User");
const Order = require("../models/Order");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// User Registration
module.exports.registerUser = (reqBody) => {

	// Data coming from User
	let newUser = new User({
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10),
		isAdmin : reqBody.isAdmin
	})

	return newUser.save().then((user, error) => {
		if (error) {
			return false;
		}
		else {
			return true;
		}
	})
}

// User Authentication/Login
module.exports.loginUser = (reqBody) => {

	return User.findOne({email : reqBody.email}).then(result => {

		if (result == null) {
			return false;
		}
		else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if (isPasswordCorrect) {
				return { access : auth.createAccessToken(result)}
			}
			else {
				return false;
			}
		}
	})
}

// List all users
module.exports.listUsers = () => {
	return User.find({}).then(result => {
		return result;
	})
}

// Set a User as Admin
module.exports.setAdmin = async (reqParams, userData) => {
	if (userData.isAdmin) {
		// Checks for User by ID
		return User.findById(reqParams).then((result, error) => {
			if (error) {
				console.log(error);
				return false;
			}

			// Setting user as admin
			result.isAdmin = true;

			return result.save().then((updateUser, err) => {
				if (err) {
					console.log(err);
					return false;
				}
				else {
					return `User has been set to Admin, isAdmin = ${updateUser.isAdmin}`;
				}
			})
		})
	}
	else {
		return "User is not an Admin"
	}
}

// Set a User as Initial Admin
module.exports.setInitialAdmin = (userId) => {
	return User.findById(userId).then((result, error) => {
		if (error) {
			console.log("error");
			return false;
		}

		result.isAdmin = true;

		return result.save().then((updateUser, err) => {
			if (err) {
				console.log(err);
				return false;
			}
			else {
				return `User has been set to Admin, isAdmin = ${updateUser.isAdmin}`;
			}
		})
	})
}

/*// List all orders (ADMIN)
module.exports.seeOrders = async (userData) => {
	if (userData.isAdmin) {
		// Checks for User by ID
		return User.findById(reqParams).then((result, error) => {
			if (error) {
				console.log(error);
				return false;
			}
			else {
				return Order.find({}).then(result => {
					return result
				})
			}
		})
	}
	else {
		return false
	}
}*/

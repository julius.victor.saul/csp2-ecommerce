const User = require("../models/User");
const Order = require("../models/Order");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Create Order
module.exports.createOrder = async (reqBody, userData) => {
		/*const result = await Product.find({ productName : data.productName}).then(order => {
			order.orders.push({products : data.productName});

			return order.save().then((order, error) => {
				if (error) {
					return false;
				}
				else {
					return true;
				}
			})
		})*/
		let newOrder = new Order({
			totalAmount : reqBody.price,
			orderUser : [{
				productId : reqBody.productId,
				products : reqBody.productName
			}]
		})

		return newOrder.save().then((order, error) => {
			if(error){
				return false;
			} else {
				return "Order has been created";
			}
		})

	}

// List all orders (ADMIN) ORDERS
module.exports.seeAdminOrders = async (reqBody, userData) => {
	if (userData.isAdmin) {
		return Order.find({}).then(result => {
			return result;
		})
	}
	else {
		return false
	}
}

// List all orders (User) MYORDERS
module.exports.seeUserOrders = async (reqBody, userData) => {
	if (userData.isAdmin == false) {
		return Order.find({}).then(result => {
			return result;
		})
	} else {
		return false
	}
}
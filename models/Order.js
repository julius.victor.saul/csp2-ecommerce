const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({

	totalAmount : {
		type : Number,
		required : [true, "Total Amount is required"]
	},
	purchasedOn : {
		type : Date,
		default : new Date()
	},
	orderUser : [{
		productId : {
			type : String,
			required : [true, "Product ID is required"]
		},
		products : {
			type : String,
			required : [true, "List of product(s) is required"]
		}
	}]

})

module.exports = mongoose.model("Order", orderSchema);
const express = require('express');
const productController = require('../controllers/product');
const auth = require('../auth');
const router = express.Router();

/* Create Product */
router.post('/', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	productController.createProduct(req.body, userData).then(result => res.send(result));
})

/* Retrieve All Active Products */
router.get("/", (req, res) => {
	productController.getAllProducts().then(result => res.send(result));
})

/* Retrieve Specific Product */
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(result => res.send(result));
})

/* Update Product */
router.put("/:productId", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	productController.updateProduct(userData, req.params).then(result => res.send(result));
})

/* Archive Product */
router.put("/:productId/archive", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	productController.archiveProduct(userData, req.params).then(result => res.send(result));
})

module.exports = router;

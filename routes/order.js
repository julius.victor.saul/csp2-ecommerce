const express = require("express");
const router = express.Router();
const orderController = require("../controllers/order");
const auth = require("../auth")

// Route to add order
router.post("/checkout",auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
	
	orderController.createOrder(req.body, userData).then(resultFromController => res.send(resultFromController));
})

// Route to list authenticate User's orders
router.get("/myOrders",auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
	orderController.seeUserOrders(req.body, userData).then(resultFromController => res.send(resultFromController));
})

// Route to list authenticate Admin's orders
router.get("/orders",auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
	orderController.seeAdminOrders(req.body, userData).then(resultFromController => res.send(resultFromController));
})

module.exports = router;

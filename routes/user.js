const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth")

// Route for registering a user
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for authenticating a user
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for setting user as a Admin
router.put("/:id/setAdmin", auth.verify, (req, res) => {
	// JWT TOKEN
	const userData = auth.decode(req.headers.authorization);
	userController.setAdmin(req.params.id, userData).then(resultFromController => res.send(resultFromController))
})

// Route for listing all users
router.get("/", (req, res) => {
	userController.listUsers(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for setting the initial admin
router.put("/setInitialAdmin/:id", (req, res) => {
	userController.setInitialAdmin(req.params.id).then(resultFromController => res.send(resultFromController));
})


module.exports = router;
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// Routes
const userRoutes = require("./routes/user");
const orderRoutes = require("./routes/order");
const productRoutes = require("./routes/product");

const app = express();

// Connect to MongoDB
mongoose.connect("mongodb+srv://dbjuliussaul:zxc_3000@wdc028-course-booking.6rxug.mongodb.net/e-commerce?retryWrites=true&w=majority",
	{	
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
)

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'));

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Defines the "users" string to be included for all user routes defined in the "user"
app.use("/user", userRoutes)

// Defines the "order" string to be include for all order routes defined in the "order" route
app.use("/order", orderRoutes)

// Defines the "product" string to be include for all product routes defined in the "product" route
app.use("/product", productRoutes)

// Listening to port
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
});
